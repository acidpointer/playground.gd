# Class to load items

#class_name ItemLoader
extends Node


const AMMO_ITEMS = {
	"ammo__5_56x45": preload("res://items/ammo__5_56x45/ammo__5_56x45.tscn"),
}

const WEAPON_ITEMS = {
	"wpn_dt_mdr_c": preload("res://items/wpn_dt_mdr_c/wpn_dt_mdr_c.tscn"),
}

func _get_instance(name: String, items_dict: Dictionary) -> Node3D:
	if items_dict.is_empty() or not items_dict.has(name):
		return null

	var item_resource = items_dict[name]
	return item_resource.instantiate()

func get_weapon_instance(weapon_name: String) -> Node3D:
	return _get_instance(weapon_name, WEAPON_ITEMS)

func get_ammo_instance(ammo_name: String) -> Node3D:
	return _get_instance(ammo_name, AMMO_ITEMS)
