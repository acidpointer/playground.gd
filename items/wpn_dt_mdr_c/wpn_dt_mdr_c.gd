extends Weapon

@onready var muzzle: Marker3D = $Muzzle
@onready var shoot_timer: Timer = $ShootTimer

@onready var rng = RandomNumberGenerator.new()

const ADS_LERP = 20

var deviation_coef = 0.1

const fview = {
	"def": 90.0,
	"ads": 50.0,
}

const TRANSFORM = {
	"weapon": {
		"position": Vector3(0.358, -0.126, -0.859),
		"ads_rotation": Vector3(-1.4, 103.5, 1.4),
		"scale": Vector3(1, 1, 1),
		"ads_position": Vector3(-0.047, -0.067, -0.568),
		"rotation": Vector3(0, 96.5, 0),
	},
	"hand_left": {
		"position": Vector3(0.024, -0.254, -0.796),
		"rotation": Vector3(6.5, -127.7, 0.7),
		"scale": Vector3(1, 1, 1),
		"ads_position": Vector3(0, 0, 0),
		"ads_rotation": Vector3(0, 0, 0),
	},
	"hand_right": {
		"position": Vector3(0.648, -0.121, -0.685),
		"rotation": Vector3(8.4, -28.6, 18.4),
		"scale": Vector3(1, 1, 1),
		"ads_position": Vector3(0, 0, 0),
		"ads_rotation": Vector3(0, 0, 0),
	},
}

func get_weapon_position() -> Vector3:
	return TRANSFORM["weapon"]["position"]

func get_weapon_rotation() -> Vector3:
	return Utils.vec_grads_to_rads(TRANSFORM["weapon"]["rotation"])

func get_hand_left_position() -> Vector3:
	return TRANSFORM["hand_left"]["position"]

func get_hand_left_rotation() -> Vector3:
	return Utils.vec_grads_to_rads(TRANSFORM["hand_left"]["rotation"])

func get_hand_right_position() -> Vector3:
	return TRANSFORM["hand_right"]["position"]

func get_hand_right_rotation() -> Vector3:
	return Utils.vec_grads_to_rads(TRANSFORM["hand_right"]["rotation"])

# Called when the node enters the scene tree for the first time.
func _ready():
	max_shoot_mode = 3

func _calc_deviation(target: Vector3):
	var coef = deviation_coef if is_aim else deviation_coef + 0.3
	
	rng.randomize()
	var x_dev = rng.randf_range(0.0, target.x * coef)
	var y_dev = rng.randf_range(0.0, target.y * coef)
	var z_dev = rng.randf_range(0.0, target.z * coef)
	return Vector3(
		target.x + x_dev,
		target.y + y_dev,
		target.z + z_dev,
	)

func _shoot():
	var bullet = bullet_instance()
	var scene = get_tree().current_scene

	scene.add_child(bullet)
	bullet.transform = muzzle.global_transform
	bullet.rotation = muzzle.global_rotation
	
	bullet.connect("explode", self._on_Bullet_Explode)
	var t = Vector3.ZERO
	if target_is_colliding:
		t = target_point - bullet.global_position
	else:
		t = bullet.transform.basis.x
	
	bullet.apply_impulse(_calc_deviation(t * bullet.SPEED))
	shoot_counter += 1

func fire_queue():
	if not shoot_timer.is_stopped():
		await shoot_timer.timeout
	
	if not is_fire:
		return

	_shoot()
	if shoot_mode == 0:
		deviation_coef = 0.1
		shoot_timer.start(0.1)
	elif shoot_mode == 1:
		deviation_coef = 0.03
		if shoot_counter >= 1:
			shoot_counter = 0
			is_fire = false
			shoot_timer.start(0.05)
			return
	elif shoot_mode == 2:
		deviation_coef = 0.034
		if shoot_counter >= 2:
			shoot_counter = 0
			is_fire = false
			shoot_timer.start(0.1)
			return
	elif shoot_mode == 3:
		deviation_coef = 0.048
		if shoot_counter >= 3:
			is_fire = false
			shoot_counter = 0
			shoot_timer.start(0.1)
			return
	if shoot_counter == 1:
		shoot_timer.start(0.1)
	fire_queue()

func _on_Bullet_Explode(body):
	#print("Collided with:", body.name)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_aim:
		var origin_q = Quaternion.from_euler(transform.origin)
		var position_q = Quaternion.from_euler(TRANSFORM["weapon"]["ads_position"])
		transform.origin = origin_q.slerp(position_q, ADS_LERP * delta).get_euler()
		if is_instance_valid(gun_camera):
			gun_camera.fov = lerp(gun_camera.fov, fview["ads"], ADS_LERP * delta)
	else:
		var origin_q = Quaternion.from_euler(transform.origin)
		var position_q = Quaternion.from_euler(TRANSFORM["weapon"]["position"])
		transform.origin = origin_q.slerp(position_q, ADS_LERP * delta).get_euler()
		if is_instance_valid(gun_camera):
			gun_camera.fov = lerp(gun_camera.fov, fview["def"], ADS_LERP * delta)
