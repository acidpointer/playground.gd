extends CharacterBody3D

@onready var head: Node3D = $Head
@onready var camera: Camera3D = $Head/Camera
@onready var aimcast: RayCast3D = $Head/Camera/Aimcast
@onready var hands: Node3D = $Head/Hands
@onready var hand_right = $Head/Hands/Hand_right
@onready var hand_left = $Head/Hands/Hand_left
@onready var gun_viewport = $Head/Camera/ViewportContainer/GunViewport
@onready var gun_camera = $Head/Camera/ViewportContainer/GunViewport/GunCamera
@onready var weapon_point = $Head/Hands/WeaponPoint



const SPEED = 8.0
const JUMP_VELOCITY = 4.5

var MOUSE_SENSIVITY = 0.03

@onready var weapon: Weapon

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var direction = Vector3.ZERO

func _ready():
	Engine.physics_jitter_fix = 0
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	equip_weapon("wpn_dt_mdr_c")

func _process(delta):
	var viewport_size = get_viewport().size * 1.5
	gun_viewport.size = viewport_size
	gun_camera.global_transform = camera.global_transform
	camera.fov = gun_camera.fov
	
	if is_instance_valid(weapon):
		weapon.target_point = aimcast.get_collision_point()
		weapon.target_normal = aimcast.get_collision_normal()
		weapon.target_is_colliding = aimcast.is_colliding()

#wpn_dt_mdr_c
func equip_weapon(weapon_name: String):
	if weapon:
		weapon_point.remove_child(weapon)
		weapon.queue_free()
	
	weapon = ItemLoader.get_weapon_instance(weapon_name)

	if is_instance_valid(weapon):
		weapon.gun_camera = gun_camera
		weapon_point.add_child(weapon)
		weapon.position = weapon.get_weapon_position()
		weapon.rotation = weapon.get_weapon_rotation()

		hand_right.position = weapon.get_hand_right_position()
		hand_right.rotation = weapon.get_hand_right_rotation()
		hand_left.position = weapon.get_hand_left_position()
		hand_right.rotation = weapon.get_hand_right_rotation()

func _input(event):
	if event is InputEventMouseMotion:
		rotate_y(deg_to_rad(-event.relative.x * MOUSE_SENSIVITY))
		head.rotate_x(deg_to_rad(-event.relative.y * MOUSE_SENSIVITY))
		head.rotation.x = clamp(head.rotation.x, deg_to_rad(-90), deg_to_rad(90))

func _physics_process(delta):
	_handle_input(delta)
	# Add the gravity
	if not is_on_floor():
		velocity.y -= gravity * delta
	
	aimcast.force_raycast_update()

func _handle_input(delta):
	if Input.is_action_just_pressed("wpn_aim"):
		weapon.is_aim = true

	if Input.is_action_just_released("wpn_aim"):
		weapon.is_aim = false

	if Input.is_action_just_pressed("wpn_fire"):
		weapon.is_fire = true
		weapon.fire_queue()
		
	if Input.is_action_just_released("wpn_fire"):
		weapon.shoot_counter = 0
		weapon.is_fire = false
		
	if Input.is_action_just_pressed("wpn_next_mode"):
		weapon.next_mode()

	# Handle Jump.
	if Input.is_action_just_pressed("move_jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	var input_dir = Input.get_vector(
		"move_left",
		"move_right",
		"move_forward",
		"move_backward"
	)
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	move_and_slide()
