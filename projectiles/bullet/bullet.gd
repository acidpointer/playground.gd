extends RigidBody3D

signal explode

@onready var light: OmniLight3D = $Light
@onready var aimcast: RayCast3D = $Aimcast
@onready var timer: Timer = $Timer

const BULLET_DECAL_RES = preload("res://projectiles/bullet/decal.tscn")

# How long (in secs) bullet instance exist
const BULLET_LIFETIME = 10

var decal_instance: Node3D

const SPEED = 300

var _collisions_count = 0

var is_hole_spawned = false

func _ready():
	timer.start(BULLET_LIFETIME)
	await timer.timeout
	if is_instance_valid(decal_instance):
		decal_instance.queue_free()
	queue_free()

func spawn_bullet_hole(location, normal, collider):
	if is_hole_spawned:
		return
	
	if not is_instance_valid(collider):
		return
	
	decal_instance = BULLET_DECAL_RES.instantiate()
	collider.add_child(decal_instance)
	decal_instance.global_transform.origin = location

	if normal == Vector3.UP:
		decal_instance.position.y = decal_instance.position.y + 0.02
		decal_instance.rotation_degrees.x = 90

	elif normal != Vector3.UP:
		decal_instance.look_at(location + normal, Vector3.UP)

	var random = RandomNumberGenerator.new()
	random.randomize()
	decal_instance.rotation_degrees.z = random.randf_range(0.0,180.0)
	is_hole_spawned = true

func _on_area_body_entered(body):
	_collisions_count += 1
	if _collisions_count == 1:
		spawn_bullet_hole(aimcast.get_collision_point(), aimcast.get_collision_normal(), aimcast.get_collider())
		explode.emit(body)
		light.queue_free()
