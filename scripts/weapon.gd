# Base class for all weapons

class_name Weapon
extends Node3D

var is_aim: bool = false
var is_fire: bool = false

# How many times weapon shoot per queue
var shoot_counter = 0

var gun_camera: Camera3D

# Bullet resource
# Common for all weapon with bullets
const BULLET_RES = preload("res://projectiles/bullet/bullet.tscn")

var shoot_mode = 0
var max_shoot_mode = 3

var target_point: Vector3 = Vector3.ZERO
var target_normal: Vector3 = Vector3.ZERO
var target_is_colliding: bool = false

func next_mode():
	shoot_mode += 1
	if shoot_mode > max_shoot_mode:
		shoot_mode = 0
	print("Set shoot mode to: ", shoot_mode)

func bullet_instance() -> RigidBody3D:
	return BULLET_RES.instantiate()

func get_weapon_position() -> Vector3:
	return Vector3.ZERO

func get_weapon_rotation() -> Vector3:
	return Vector3.ZERO

func get_hand_left_position() -> Vector3:
	return Vector3.ZERO

func get_hand_left_rotation() -> Vector3:
	return Vector3.ZERO

func get_hand_right_position() -> Vector3:
	return Vector3.ZERO

func get_hand_right_rotation() -> Vector3:
	return Vector3.ZERO
