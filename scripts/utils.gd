extends Node

func vec_grads_to_rads(vec: Vector3) -> Vector3:
	return Vector3(deg_to_rad(vec.x), deg_to_rad(vec.y), deg_to_rad(vec.z))
